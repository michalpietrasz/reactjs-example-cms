var React = require('react');
var ReactRouter = require('react-router');
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var IndexRoute = ReactRouter.IndexRoute;
var hashHistory = ReactRouter.hashHistory;
var Home = require('../components/Home');
var Layout = require('../components/Layout');
var AddDocument = require('../components/AddDocument');
var EditDocument = require('../components/EditDocument');

var routes = (
  <Router history={hashHistory}>
    <Route path="/" component={Layout} >
      <IndexRoute component={Home} />
      <Route path="add" component={AddDocument} />
      <Route path="/:id/edit" component={EditDocument} />
    </Route>
  </Router>
);

module.exports = routes;
