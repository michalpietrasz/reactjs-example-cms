'use strict';

var React = require('react');
var Link = require('react-router').Link;
var ListContainer = require('../containers/ListContainer');

function Home() {
  return (
    <div>
      <div className="jumbotron">
        <h1>Documents</h1>
        <p>
          <Link to="add">
            <button className="btn btn-primary btn-md">Add</button>
          </Link>
        </p>
      </div>
      <ListContainer />
    </div>
  );
}

module.exports = Home;
