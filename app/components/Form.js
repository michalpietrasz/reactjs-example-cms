'use strict';

var React = require('react');
var Link = require('react-router').Link;
var PropTypes = React.PropTypes;

function Form (props) {
  return (
    <form className="form-horizontal" method="POST" onSubmit={props.submitForm}>
      <div className="form-group">
        <label htmlFor="" className="col-sm-2">Title</label>
        <div className="col-sm-10">
          <input type="text" name="title" className="form-control" required="true" onChange={props.submitChange} value={props.object.title} />
        </div>
      </div>
      <div className="form-group">
        <label htmlFor="" className="col-sm-2">Description</label>
        <div className="col-sm-10">
          <textarea name="description" className="form-control" rows="10" required="true" onChange={props.submitChange} value={props.object.description} />
        </div>
      </div>
      <div className="form-group">
        <div className="col-sm-10 col-sm-offset-2">
          <Link to="/">
            <button type="reset" className="btn btn-default">Cancel</button>
          </Link>
          <button type="submit" className="btn btn-primary">Submit</button>
        </div>
      </div>
    </form>
  );
}

Form.propTypes = {
  submitForm: PropTypes.func.isRequired,
  submitChange: PropTypes.func.isRequired,
  object: PropTypes.object.isRequired
};


module.exports = Form;
