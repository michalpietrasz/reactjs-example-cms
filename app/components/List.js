'use strict';

var React = require('react');
var PropTypes = React.PropTypes;
var Link = require('react-router').Link;

var List = React.createClass({
  render: function (props) {
    var list = [];
    this.props.documents.map(function (item) {
      list.push
      (
        <div className="well" key={item.id}>
          <h4 className="title">
            {item.title}
            <Link to={item.id + '/edit'}>
              <span className="btn btn-sm btn-success pull-right"><i className="fa fa-edit"></i></span>
            </Link>
            <span className="btn btn-sm btn-danger pull-right" onClick={this.props.onRemove.bind(null, item.id)}><i className="fa fa-trash"></i></span>
          </h4>
          <p className="text-muted">
            {item.description}
            {item.description.split('\n').map(function (item, key) {
              return (
                <span key={key}>{item}<br/></span>
              )
            })}
          </p>
        </div>
      )
    }.bind(this));
    return (
      <div>
        {list}
      </div>
    )
  }
});

List.propTypes = {
  onRemove: PropTypes.func.isRequired,
  documents: PropTypes.array.isRequired
};

module.exports = List;
