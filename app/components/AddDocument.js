'use strict';

var React = require('react');
var Link = require('react-router').Link;
var FormContainer = require('../containers/FormContainer');

function AddDocument () {
  return (
    <div>
      <div className="jumbotron">
        <h1>Add new document</h1>
        <p>
          <Link to="/">
            <button className="btn btn-primary btn-md">Back to list</button>
          </Link>
        </p>
      </div>
      <div className="well bs-component">
        <FormContainer />
      </div>
    </div>
  )
}

module.exports = AddDocument;
