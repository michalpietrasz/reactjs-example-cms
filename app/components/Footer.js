'use strict';

var React = require('react');

var Footer = React.createClass({
  render: function () {
    return (
      <footer>
        <div className="container">
          <div className="copyright">
            @2017
          </div>
        </div>
      </footer>
    );
  }
});

module.exports = Footer;
