'use strict';

var React = require('react');
var Header = require('./Header');
var Footer = require('./Footer');

var Layout = React.createClass({
  render: function () {
    return (
      <div>
        <Header />
        <div className="container">
          {this.props.children}
        </div>
        <Footer />
      </div>
    );
  }
});

module.exports = Layout;
