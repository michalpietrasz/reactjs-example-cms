'use strict';

var React = require('react');
var List = require('../components/List');
var storage = require('../utils/storage');

var ListContainer = React.createClass({
  handleRemove: function(id) {
    if(confirm('Delete the item?')) {
      storage.remove(id);
      this.forceUpdate();
    }
  },
  getDocuments: function() {
    return (
      <div>
        <List
          onRemove={this.handleRemove}
          documents={storage.getAll()}
        />
      </div>
    )
  },
  render: function () {
    return (
      <div>{this.getDocuments()}</div>
    );
  }
});

module.exports = ListContainer;
