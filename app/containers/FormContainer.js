'use strict';

var React = require('react');
var Form = require('../components/Form');
var storage = require('../utils/storage');

var FormContainer = React.createClass({
  contextTypes: {
    router: React.PropTypes.object.isRequired
  },
  getInitialState: function() {
    var object = {};
    if (this.props.id) {
      object = storage.getDocumentById(this.props.id);
    }

    return {
      id: object ? object.id : null,
      title: object ? object.title : null,
      description: object ? object.description : null,
    }
  },
  handleSubmit: function(e) {
    e.preventDefault();
    storage.update(this.state);
    this.context.router.push('/');
  },
  handleChange: function(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  },
  render: function () {
    return (
      <div>
        <Form
          submitForm={this.handleSubmit}
          submitChange={this.handleChange}
          object={this.state}
        />
      </div>
    );
  }
});

module.exports = FormContainer;
