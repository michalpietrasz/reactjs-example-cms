'use strict';

var storage = {
  key: 'documents',
  getAll() {
    return JSON.parse(localStorage.getItem('documents')) || [];
  },
  getDocumentById(id) {
    var objects = this.getAll();
    var key = this.extractKey(objects, id);
    return objects[key];
  },
  update(object) {
    var documents = this.getAll();
    if(!object.id) {
      object['id'] = new Date().getTime();
      documents.push(object);
    } else {
      var key = this.extractKey(documents, object['id']);
      documents[key] = object;
    }

    localStorage.setItem('documents', JSON.stringify(documents));
  },
  remove(id) {
    var objects = this.getAll();
    var key = this.extractKey(objects, id);
    console.log(key);
    objects.splice(key,1);
    localStorage.setItem(this.key, JSON.stringify(objects));
  },
  extractKey(obj, value) {
    for(var i = 0; i < Object.keys(obj).length; i++) {
      if(obj[i]['id'] == value) {
        return i;
      }
    }
  },
}

module.exports = storage;
